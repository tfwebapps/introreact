import TrainerCard from "./TrainerCard"

//Autre façon de faire -> Mettre le composant ici s'il ne sert qu'à ça -> "Pas de réusabilité"
// const TrainerCard = ({ firstname, lastname, birthdate }) => {
//     return (
//         <div>
//             <h3>{firstname} {lastname}</h3>
//             <p>{ birthdate.toLocaleDateString('fr-BE', { weekday : 'long', year : 'numeric', month : '2-digit', day:'2-digit'}) }</p>
//         </div>
//     )
// }

const Trainers = () => {

    const trainers = [
        { id : 1, lastname : "Beurive", firstname : "Aude", birthdate : new Date(1989, 9, 16)},
        { id : 2, lastname : "Chaineux", firstname : "Gavin", birthdate : new Date(1993, 9, 18)},
        { id : 3, lastname : "Ly", firstname : "Khun", birthdate : new Date(1982, 4, 6)},
    ]

    return (
        <div>
            { trainers.map(trainer => <TrainerCard key={trainer.id} firstname={trainer.firstname} lastname={trainer.lastname} birthdate={trainer.birthdate} />)}
        </div>
    )
}

export default Trainers