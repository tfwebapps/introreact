
const TrainerCard = ({ firstname, lastname, birthdate }) => {
    return (
        <div>
            <h3>{firstname} {lastname}</h3>
            <p>{ birthdate.toLocaleDateString('fr-BE', { weekday : 'long', year : 'numeric', month : '2-digit', day:'2-digit'}) }</p>
        </div>
    )
}

export default TrainerCard

