
const ProductList = () => {

    const productList = ["Patates", "Hâché", "Salade"]

    return (
        <>
            <ul>
                { productList.map( (product, index) => <li key={index}>{product}</li>) }
            </ul>
        </>
    )
}

export default ProductList