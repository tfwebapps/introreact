import logo from './logo.svg';
import './App.css';
import Hello from './Demo1-Component/Hello';
import HelloSomeone from './Demo2-ComponentWithProps/HelloSomeone';
import Pet from './Demo3-Conditions/Pet';
import ProductList from './Demo4-Collections/ProductList';
import Trainers from './Demo4-Collections/Trainers';
import ShoppingList from './Exo1-ShoppingList/ShoppingList';
import CountNotWorks from './Demo5-EventsEtState/CountNotWorks';
import CountWorks from './Demo5-EventsEtState/CountWorks';
import FormSimple from './Demo6-Formulaires/FormSimple';
import FormWithLib from './Demo6-Formulaires/FormWithLib';
import ShoppingList2 from './Exo2-ShoppingListAdvanced/ShoppingList2';

function App() {

  const productList = [
    { id : 1, name : "Calzone", price : 10.50, promo : false, pricePromo : 9.50, image : "./images/calzone.webp", available : true},
    { id : 2, name : "Margherita", price : 8.50, promo : true, pricePromo : 7.50, image : "./images/margherita.webp", available : true},
    { id : 3, name : "Quatre Fromages", price : 12.50, promo : false, pricePromo : 10.50, image : "./images/quatrefromage.jpg", available : true},
    { id : 4, name : "Regina", price : 11.75, promo : false, pricePromo : 10, image : "./images/regina.jpeg", available : false}
  ]

  return (
    <>
     {/* <Hello />
     <HelloSomeone firstname="Aude" lastname="Beurive" />
     <HelloSomeone firstname="Steve" />
     <HelloSomeone /> */}

     {/* Conditions */}
     {/* <Pet name="Rantanplan" isDog={true} havePet={true} />
     <Pet isDog={true} havePet={true} />
     <Pet name="Garfield" isDog={false} havePet={true} />
     <Pet havePet={false} /> */}

     {/* Collections */}
     {/* <ProductList />
     <Trainers /> */}

    {/* Exercice 1 */}
    {/* <ShoppingList productList={productList} /> */}

    {/* Events */}
    {/* <CountNotWorks />
    <CountWorks /> */}

    {/* Formulaires */}
    {/* <FormSimple />
    <FormWithLib /> */}

    {/* Exercice 2 */}
    <ShoppingList2 />


    </>
  );
}

export default App;
