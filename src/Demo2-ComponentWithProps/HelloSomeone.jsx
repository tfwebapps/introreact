import PropTypes from "prop-types"
//1ère façon d'importer du css
import "./HelloSomeone.css"
//2ème façon d'importer du css -> Via module
import style from "./HelloSomeone.module.css"

const HelloSomeone = (props) => {
    //props => un objet qui contient toutes les infos passées lors de l'appel du component
    const {firstname, lastname} = props

    return (
        <div>
            <h2 className="red">Bonjour {firstname} {lastname}</h2>
            <h3 className={style.red}>Enjoy cette découverte de React</h3>
        </div>
    )
}

//Pour setup des valeurs aux props
HelloSomeone.defaultProps = {
    firstname : "Robert",
    lastname : "Dupond"
}

//Pour définir les types des props :
// Installer prop-types dans le projet : npm install prop-types
HelloSomeone.propTypes = {
    firstname : PropTypes.string,
    lastname : PropTypes.string
}

export default HelloSomeone