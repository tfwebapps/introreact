import ProductCard2 from "./ProductCard2"
import style from "../Exo1-ShoppingList/Shopping.module.css"
import { useState } from "react"
import { useForm } from "react-hook-form"

  const ShoppingList2 = () => {

    const [productList, setProductList] = useState([
        { id : 1, name : "Calzone", price : 10.50, promo : false, pricePromo : 9.50, image : "./images/calzone.webp", available : true},
        { id : 2, name : "Margherita", price : 8.50, promo : true, pricePromo : 7.50, image : "./images/margherita.webp", available : true},
        { id : 3, name : "Quatre Fromages", price : 12.50, promo : false, pricePromo : 10.50, image : "./images/quatrefromage.jpg", available : true},
        { id : 4, name : "Regina", price : 11.75, promo : false, pricePromo : 10, image : "./images/regina.jpeg", available : false}  
    ])

    const {register, formState : { errors }, handleSubmit, reset} = useForm({ mode : "onBlur"})
    
    const submitForm = (data) => {
        console.log(data);
        // * Il va falloir générer un nouvel id
        // Pour ça, on peut récupérer le max parmi ceux qu'il y a déjà et lui rajouter 1
        //productList.map(product => product.id) -> Renvoie un tableau avec juste les id
        if(productList.length > 0) {
            let idMax = Math.max(...productList.map(product => product.id))
            data.id = idMax + 1
        }
        else {
            data.id = 1
        }
        console.log(data);

        // * Ajout dans la liste
        setProductList(previousList => [...previousList, data])
        //setProductList([...productList, data])
        reset()
    }

    const onErrorForm = (errors) => {
        console.log(errors);
        // ? Si vous voulez gérer un p'tit warnig avec récap des erreurs, ce sera par ici
    }

    const deleteInList = (id) => {
        console.log("Suppression de ", id);
        //On filtre la liste pour obtenir la liste sans le produit dont l'id est égal à celui reçu en paramètre
        setProductList(productList.filter(product => product.id !== id))
    }

    return (
        <>
            <form onSubmit={handleSubmit(submitForm, onErrorForm)}>
                <h2>Ajout d'une pizza au menu :</h2>
                <div>
                    <label htmlFor="name">Nom : </label>
                    <input id="name" type="text" { ...register("name", { required : true, maxLength : 250})} />
                    { errors.name?.type === "required" && <span className="txt-error">Ce champs est requis</span>}
                    { errors.name?.type === "maxLength" && <span className="txt-error">Ce champs doit faire max 250 caractères</span>}
                </div>
                <div>
                    <label htmlFor="image">Image : </label>
                    <input id="image" type="url" { ...register("image")} />
                </div>
                <div>
                    <label htmlFor="price">Prix : </label>
                    <input id="price" type="number" step="0.01" min="0" { ...register("price", { required : true, min : 0, max : 30})}/>
                    { errors.price?.type === "required" && <span className="txt-error">Ce champs est requis</span>}
                    { errors.price?.type === "min" && <span className="txt-error">La valeur minimale est de 0</span>}
                    { errors.price?.type === "max" && <span className="txt-error">La valeur maximale est de 30</span>}
                </div>
                <div>
                    <label htmlFor="price-promo">Prix quand promo : </label>
                    <input id="price-promo" type="number" step="0.01" min="0" { ...register("pricePromo", { required : true, min : 0, max : 30})}/>
                    { errors.pricePromo?.type === "required" && <span className="txt-error">Ce champs est requis</span>}
                    { errors.pricePromo?.type === "min" && <span className="txt-error">La valeur minimale est de 0</span>}
                    { errors.pricePromo?.type === "max" && <span className="txt-error">La valeur maximale est de 30</span>}
                </div>
                <div>
                    <input id="promo" type="checkbox" { ...register("promo")} />
                    <label htmlFor="promo"> En promo ?</label>
                </div>
                <div>
                    <input type="checkbox" id="available" { ...register("available")} />
                    <label htmlFor="available"> Disponible ?</label>
                </div>
                <div>
                    <input type="submit" value="Ajouter" />
                </div>
            </form>

            <div className={style.container}>
                { productList.map( product => <ProductCard2 key={product.id} product={product} onDelete={deleteInList} />)}
            </div>
        </>
    )

}

export default ShoppingList2