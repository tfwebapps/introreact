import style from "../Exo1-ShoppingList/Shopping.module.css"

const ProductCard2 = ({ product, onDelete }) => {
    const {id, name, image, promo, price, pricePromo, available} = product

    const handleSupp = () => {
        onDelete(id)
    }

    if(!available) {
        return (
            <div className={style.card}>
                <h3>{name}</h3>
                <img className={style.imgcard} src="./images/outofstock.png" alt="produit plus en stock" />
                <button onClick={handleSupp}>Supprimer</button>
            </div>

        )
    }


    return (
        <div className={style.card}>
            <h3>{name}</h3>
            <img className={style.imgcard} src={image} alt={'pizza ' + name} />
            {
                (promo) ? 
                <p><span className={style.priceBefore}>{price} €</span> <span className={style.priceAfter}>{pricePromo} €</span></p>
                :
                <p>{price} €</p>
            }
            <button onClick={handleSupp}>Supprimer</button>

        </div>
    )
}

export default ProductCard2