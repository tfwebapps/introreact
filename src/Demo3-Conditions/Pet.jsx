import "../Demo2-ComponentWithProps/HelloSomeone"

const Pet = (props) => {
    const {name, isDog, havePet} = props

    //Si pas d'animal, on renvoie ce rendu ci
    if(!havePet) {
        return (
            <>
                <p>Pas de petit compagnon ? <a href="https://www.sanscollier.be/">Cliquez-ici pour en adopter un </a></p>
            </>
        )
    }
    
    //Si animal, on renvoie celui ci
    return (
        <div>
            {/* 2ème façon de faire les conditions */}
            {
                (isDog) ? 
                <img style={{"width":"80px"}} src="./images/dog.png" alt="icon de potichien" /> 
                : 
                <img style={{"width":"80px"}} src="./images/cat.png" alt="icon de poticha" />
            }
            { /* 3ème façon de faire les conditions */ }
            {
                // Vérifie si name n'a pas une valeur "falsie"
                // -> si name n'est ni undefined, null, false
                (name) &&
                <p className={isDog? 'red' : ''}>Votre compagnon s'appelle {name}</p>
            }
            
            {/* {
                //Affiche name si pas null, sinon pas de compagnon 
                <p>{ name ?? "Pas de nom"}</p>
            }  */}

            {/* { 
                <p>{(name) || "Pas de nom"} </p>               
            } */}
        </div>
    )
    
}

export default Pet