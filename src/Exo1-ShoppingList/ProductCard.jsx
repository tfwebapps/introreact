import style from "./Shopping.module.css"

const ProductCard = ({ product }) => {
    const {name, image, promo, price, pricePromo, available} = product

    if(!available) {
        return (
            <div className={style.card}>
                <h3>{name}</h3>
                <img className={style.imgcard} src="./images/outofstock.png" alt="produit plus en stock" />
            </div>
        )
    }

    return (
        <div className={style.card}>
            <h3>{name}</h3>
            <img className={style.imgcard} src={image} alt={'pizza ' + name} />
            {
                (promo) ? 
                <p><span className={style.priceBefore}>{price} €</span> <span className={style.priceAfter}>{pricePromo} €</span></p>
                :
                <p>{price} €</p>
            }
        </div>
    )
}

export default ProductCard