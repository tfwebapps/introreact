import ProductCard from "./ProductCard"
import style from "./Shopping.module.css"

const ShoppingList = (props) => {
    const { productList } = props
    
    return (
        <div className={style.container}>
            { productList.map( product => <ProductCard key={product.id} product={product} />)}
        </div>
    )

}

export default ShoppingList