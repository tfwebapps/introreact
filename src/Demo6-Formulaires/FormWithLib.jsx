import { useForm } from "react-hook-form"


const FormWithLib = () => {

    const {register, handleSubmit, formState : { errors }} = useForm({ mode : 'onBlur' })
    //register -> Ce qui va nous permettre de lier un input à une variable
    //handlesubmit -> Fonction déjà toute faite, qui comprend le preventDefault de l'event submit et "récolte" toutes les données register et en fait un objet qu'il envoie à la fonction fournie
    //formState -> Nous permet de savoir l'état du formulaire (valid/invalid) et les différents messages d'erreurs

    const submitForm = (data) => {
        console.log(data)
        //TODO : Ajouter dans une liste ou en db
    }

    return (
        <div>
            <form onSubmit={handleSubmit(submitForm)}>
                <div>
                    <label htmlFor="firstname">Votre prénom : </label>
                    <input id="firstname" type="text" {...register("firstname", { maxLength : 25 })}/>
                    { errors.firstname?.type === 'maxLength' && <span className="txt-error">Ce champ ne peut excéder 25 caractères</span> }
                </div> 
                <div>
                    <label htmlFor="msg">Votre message : </label>
                    <textarea name="" id="" cols="30" rows="10" {...register("msg", { required : true, minLength : 2, maxLength : 250})}/>
                    { errors.msg?.type === 'required' && <span className="txt-error">Ce champ est requis</span> }
                    { errors.msg?.type === 'minLength' && <span className="txt-error">Le message doit faire au moins 2 caractères</span> }
                    { errors.msg?.type === 'maxLength' && <span className="txt-error">Le message doit faire au maximum 250 caractères</span> }
                </div>
                <div>
                    <input id="ano" type="checkbox" {...register("anonymous")}/>
                    <label htmlFor="ano"> Voulez-vous poster vote message en anonyme ?</label>
                </div>
                <div>
                    <input type="submit" value="Envoyer le message"/>
                </div>
            </form>
        </div>
    )
}

export default FormWithLib