import { useState } from "react";

const FormSimple = () => {
    const [firstname, setFirstname] = useState("")
    const [msg, setMsg] = useState("")
    const [anonymous, setAnonymous] = useState(false)

    const submitForm = (event) => {
        event.preventDefault(); //Obligatoire et toujours en premier pour supprimer le comportement par défaut de l'event submit qui envoie le formulaire et recharge la page (+ vide le form)

        console.log(firstname);
        console.log(msg);
        console.log(anonymous);
        
        console.log("Envoyé");
    }

    const changeName = (event) => {
        // console.log(event.target.value);
        setFirstname(event.target.value)
    }

    return (
        <div>
            <form onSubmit={submitForm}>
                <div>
                    {/* Pour relier label à son input, ce n'est plus for, c'est htmlFor */}
                    <label htmlFor="firstname">Votre prénom : </label>
                    <input id="firstname" type="text" value={firstname} onChange={changeName} />
                </div>
                <div>
                    <label htmlFor="msg">Votre message : </label>
                    { /* textarea devient une balise orpheline en react */ }
                    <textarea name="" id="" cols="30" rows="10" value={msg} onChange={(e) => setMsg(e.target.value) } />
                </div>
                <div>
                    {/* Attention pour les checkbox, la variable va être reliée via l'attribut checked et non value */}
                    <input id="ano" type="checkbox" checked={anonymous} onChange={(e) => setAnonymous(e.target.checked)} />
                    <label htmlFor="ano"> Voulez-vous poster vote message en anonyme ?</label>
                </div>
                <div>
                    <input type="submit" value="Envoyer le message"/>
                </div>

            </form>
        </div>
    )
}

export default FormSimple