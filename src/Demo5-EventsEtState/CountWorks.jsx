import { useState } from "react"

const CountWorks = () => {
    //Utilisation de la hook useState
    const [count, setCount]  = useState(0)
    //On extrait de useState :
    //en premier : La variable qu'on veut utiliser (afficher, modifier etc)
    //en deuxième : La méthode qui va nous permettre de modifier cette variable (convention set+nomVariable)

    //let count = 0
    //let count -> remplacé par count récupéré de useState
    //initialisation à 0 (=0) -> remplacé par l'initialisation dans les paramètres de useState

    const increment = () => {
        // count++
        //setCount(45) //On peut mettre une seule valeur si on a besoin de set la variable à une valeur arbitraire
        //setCount( prev => nouvelleValeur) //fonction anonyme si besoin de l'ancienne valeur
        setCount(previous => previous + 1)
        console.log(count);
    }

    return (
        <div>
            <p>Le compteur est à { count } </p>
            <button onClick={increment} >+1</button>
        </div>
    )
}

export default CountWorks