
const CountNotWorks = () => {

    let count = 0
    const increment = () => {
        count++
        console.log(count);
    }

    return (
        <div>
            <p>Le compteur est à { count } </p>
            <button onClick={increment} >+1</button>
        </div>
    )
}

export default CountNotWorks